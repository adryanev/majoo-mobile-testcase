import 'package:majootestcase/app/app.dart';
import 'package:majootestcase/bootstrap.dart';

Future<void> main() async {
  await bootstrap(() => const App());
}
