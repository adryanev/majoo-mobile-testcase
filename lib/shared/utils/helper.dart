import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:majootestcase/injector.dart';

final logger = getIt<Logger>();

extension BuildContextX on BuildContext {
  void showSnackBar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(SnackBar(content: Text(message)));
  }
}
