import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:majootestcase/l10n/l10n.dart';

enum PasswordValidationError { empty, subceedingLength }

class FieldPassword extends FormzInput<String, PasswordValidationError> {
  const FieldPassword.pure() : super.pure('');
  const FieldPassword.dirty([String value = '']) : super.dirty(value);

  @override
  PasswordValidationError? validator(String value) {
    return value.isNotEmpty == true
        ? (value.length < 6 ? PasswordValidationError.subceedingLength : null)
        : PasswordValidationError.empty;
  }
}

extension PasswordValidationX on PasswordValidationError {
  String description(BuildContext context) {
    final i10n = context.l10n;
    switch (this) {
      case PasswordValidationError.empty:
        final field = i10n.password;
        return i10n.cannotBeBlank(field);
      case PasswordValidationError.subceedingLength:
        return i10n.passwordSubcceedLength;
      default:
        return '';
    }
  }
}
