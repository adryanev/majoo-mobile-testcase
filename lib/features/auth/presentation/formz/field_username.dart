import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:majootestcase/l10n/l10n.dart';

enum UsernameValidationError { empty, invalid }

class FieldUsername extends FormzInput<String, UsernameValidationError> {
  const FieldUsername.pure([String value = '']) : super.pure(value);

  const FieldUsername.dirty([String value = '']) : super.dirty(value);

  @override
  UsernameValidationError? validator(String value) {
    return value.isEmpty
        ? UsernameValidationError.empty
        : value.contains('\n')
            ? UsernameValidationError.invalid
            : null;
  }
}

extension UsernameValidationErrorX on UsernameValidationError {
  String description(BuildContext context) {
    final i10n = context.l10n;
    final field = i10n.username;
    switch (this) {
      case UsernameValidationError.invalid:
        return i10n.mustSingleLine(field);
      case UsernameValidationError.empty:
        return i10n.cannotBeBlank(field);
    }
  }
}
