import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:formz/formz.dart';
import 'package:go_router/go_router.dart';
import 'package:majootestcase/app/routes/route_list.dart';
import 'package:majootestcase/features/auth/presentation/blocs/auth_status_bloc/auth_status_bloc.dart';
import 'package:majootestcase/features/auth/presentation/blocs/login_form/login_form_bloc.dart';
import 'package:majootestcase/features/auth/presentation/formz/custom_form.dart';
import 'package:majootestcase/injector.dart';
import 'package:majootestcase/l10n/l10n.dart';
import 'package:majootestcase/shared/utils/helper.dart';
import 'package:majootestcase/shared/widgets/auth_text_input.dart';
import 'package:majootestcase/shared/widgets/custom_button.dart';
import 'package:majootestcase/shared/widgets/my_button.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthStatusBloc, AuthStatusState>(
      listener: (context, state) {
        state.maybeWhen(
            authenticated: () {
              logger.d("authenticated");
              context.go(movie);
            },
            orElse: () {});
        if (state is AuthStatusAuthenticated) {}
      },
      child: Scaffold(
        body: BlocProvider<LoginFormBloc>(
          create: (context) => getIt<LoginFormBloc>(),
          child: BlocConsumer<LoginFormBloc, LoginFormState>(
            listener: (context, state) {
              state.authFailureOrSuccessOption.fold(
                () => null,
                (either) => either.fold(
                  (l) => l.when(userNotFound: () {
                    context.showSnackBar(context.l10n.userNotFound);
                  }, cannotCreateUser: () {
                    context.showSnackBar(context.l10n.cannotCreateUser);
                  }, sessionFailure: () {
                    context.showSnackBar(context.l10n.sessionFailure);
                  }, unexpectedFailure: () {
                    context.showSnackBar(context.l10n.unexpectedFailure);
                  }),
                  (r) {
                    context
                      ..showSnackBar(context.l10n.loginSuccess)
                      ..go(movie);
                  },
                ),
              );
            },
            builder: (context, state) => SingleChildScrollView(
              child: Padding(
                padding:
                    EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Selamat Datang',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    Text(
                      'Silahkan login terlebih dahulu',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    LoginFormWidget(),
                    SizedBox(
                      height: 50,
                    ),
                    if (state.formStatus.isValid && (!state.isSubmitting))
                      MyGradientButton(
                        text: context.l10n.login,
                        onPressed: () {
                          context
                              .read<LoginFormBloc>()
                              .add(LoginFormEvent.loginButtonPressed());
                        },
                        height: 40.h,
                      )
                    else
                      MyWhiteButton(
                        text: context.l10n.login,
                        onPressed: null,
                        height: 40.h,
                      ),
                    SizedBox(
                      height: 50,
                    ),
                    _register(context),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _register(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.push(register);
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }
}

class LoginFormWidget extends StatelessWidget {
  const LoginFormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const _EmailInput(),
        SizedBox(height: 10.h),
        const _PasswordInput(),
        SizedBox(height: 10.h),
      ],
    );
  }
}

class _EmailInput extends StatelessWidget {
  const _EmailInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return BlocBuilder<LoginFormBloc, LoginFormState>(
      buildWhen: (previous, current) =>
          previous.fieldEmailAddress != current.fieldEmailAddress,
      builder: (context, state) => AuthTextInput(
        key: const Key('login_email_input'),
        isPassword: false,
        hintText: l10n.email,
        errorText: state.fieldEmailAddress.invalid
            ? state.fieldEmailAddress.error?.description(context)
            : null,
        onChanged: (emailAddress) {
          emailAddress.trim();
          context.read<LoginFormBloc>().add(
                LoginFormEvent.emailChanged(emailAddress),
              );
        },
      ),
    );
  }
}

class _PasswordInput extends StatelessWidget {
  const _PasswordInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return BlocBuilder<LoginFormBloc, LoginFormState>(
      buildWhen: (previous, current) =>
          previous.fieldPassword != current.fieldPassword,
      builder: (context, state) => AuthTextInput(
        key: const Key('login_password_input'),
        isPassword: true,
        hintText: l10n.password,
        errorText: state.fieldPassword.invalid
            ? state.fieldPassword.error?.description(context)
            : null,
        onChanged: (password) {
          password.trim();
          context.read<LoginFormBloc>().add(
                LoginFormEvent.passwordChanged(password),
              );
        },
      ),
    );
  }
}
