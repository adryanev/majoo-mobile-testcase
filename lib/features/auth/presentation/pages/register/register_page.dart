import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:formz/formz.dart';
import 'package:go_router/go_router.dart';
import 'package:majootestcase/app/routes/route_list.dart';
import 'package:majootestcase/features/auth/presentation/blocs/register_form/register_form_bloc.dart';
import 'package:majootestcase/features/auth/presentation/formz/custom_form.dart';
import 'package:majootestcase/injector.dart';
import 'package:majootestcase/l10n/l10n.dart';
import 'package:majootestcase/shared/utils/helper.dart';
import 'package:majootestcase/shared/widgets/auth_text_input.dart';
import 'package:majootestcase/shared/widgets/my_button.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<RegisterFormBloc>(
        create: (context) => getIt<RegisterFormBloc>(),
        child: BlocConsumer<RegisterFormBloc, RegisterFormState>(
          listener: (context, state) {
            state.loginFailureOrSuccessOption.fold(
              () => null,
              (either) => either.fold(
                (l) => l.when(
                  userNotFound: () {
                    context.showSnackBar(context.l10n.userNotFound);
                  },
                  cannotCreateUser: () {
                    context.showSnackBar(context.l10n.cannotCreateUser);
                  },
                  sessionFailure: () {
                    context.showSnackBar(context.l10n.sessionFailure);
                  },
                  unexpectedFailure: () {
                    context.showSnackBar(context.l10n.unexpectedFailure);
                  },
                ),
                (r) {
                  context
                    ..showSnackBar(context.l10n.registerSuccess)
                    ..go(movie);
                },
              ),
            );
          },
          builder: (context, state) => SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Silahkan Register terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  RegisterFormWidget(),
                  SizedBox(
                    height: 50,
                  ),
                  if (state.formStatus.isValid && (!state.isSubmitting))
                    MyGradientButton(
                      text: context.l10n.register,
                      onPressed: () {
                        context
                            .read<RegisterFormBloc>()
                            .add(RegisterFormEvent.registerButtonPressed());
                      },
                      height: 40.h,
                    )
                  else
                    MyWhiteButton(
                      text: context.l10n.register,
                      onPressed: null,
                      height: 40.h,
                    ),
                  SizedBox(
                    height: 50,
                  ),
                  _login(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _login(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.go(login);
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }
}

class RegisterFormWidget extends StatelessWidget {
  const RegisterFormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const _UsernameInput(),
        SizedBox(height: 10.h),
        const _EmailInput(),
        SizedBox(height: 10.h),
        const _PasswordInput(),
        SizedBox(height: 10.h),
      ],
    );
  }
}

class _EmailInput extends StatelessWidget {
  const _EmailInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return BlocBuilder<RegisterFormBloc, RegisterFormState>(
      buildWhen: (previous, current) =>
          previous.fieldEmailAddress != current.fieldEmailAddress,
      builder: (context, state) => AuthTextInput(
        key: const Key('Register_email_input'),
        isPassword: false,
        hintText: l10n.email,
        errorText: state.fieldEmailAddress.invalid
            ? state.fieldEmailAddress.error?.description(context)
            : null,
        onChanged: (emailAddress) {
          emailAddress.trim();
          context.read<RegisterFormBloc>().add(
                RegisterFormEvent.emailChanged(emailAddress),
              );
        },
      ),
    );
  }
}

class _UsernameInput extends StatelessWidget {
  const _UsernameInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return BlocBuilder<RegisterFormBloc, RegisterFormState>(
      buildWhen: (previous, current) =>
          previous.fieldUsername != current.fieldUsername,
      builder: (context, state) => AuthTextInput(
        key: const Key('register_username_input'),
        isPassword: false,
        hintText: l10n.username,
        errorText: state.fieldUsername.invalid
            ? state.fieldUsername.error?.description(context)
            : null,
        onChanged: (username) {
          username.trim();
          context.read<RegisterFormBloc>().add(
                RegisterFormEvent.usernameChanged(username),
              );
        },
      ),
    );
  }
}

class _PasswordInput extends StatelessWidget {
  const _PasswordInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return BlocBuilder<RegisterFormBloc, RegisterFormState>(
      buildWhen: (previous, current) =>
          previous.fieldPassword != current.fieldPassword,
      builder: (context, state) => AuthTextInput(
        key: const Key('Register_password_input'),
        isPassword: true,
        hintText: l10n.password,
        errorText: state.fieldPassword.invalid
            ? state.fieldPassword.error?.description(context)
            : null,
        onChanged: (password) {
          password.trim();
          context.read<RegisterFormBloc>().add(
                RegisterFormEvent.passwordChanged(password),
              );
        },
      ),
    );
  }
}
