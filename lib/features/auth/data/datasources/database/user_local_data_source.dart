import 'package:injectable/injectable.dart';

import 'package:majootestcase/features/auth/data/models/user_model.dart';
import 'package:majootestcase/features/auth/data/services/dao/auth_dao.dart';

abstract class UserLocalDataSource {
  Future<int> insertUser(UserModel model);
  Future<UserModel?> findUserByEmailAndPassword(String email, String password);
}

@LazySingleton(as: UserLocalDataSource)
class UserLocaDataSourceImpl implements UserLocalDataSource {
  UserLocaDataSourceImpl(this.authDao);
  final AuthDao authDao;

  @override
  Future<UserModel?> findUserByEmailAndPassword(String email, String password) {
    return authDao.findUserByEmailAndPassword(email, password);
  }

  @override
  Future<int> insertUser(UserModel model) {
    return authDao.insertUser(model);
  }
}
