import 'package:injectable/injectable.dart';
import 'package:majootestcase/features/auth/data/datasources/preference/auth_shared_preference.dart';

abstract class AuthPreferenceLocalDataSource {
  Future<bool> setLoggedIn();
  Future<bool> setLoggedOut();
  Future<bool?> getLogInStatus();
}

@LazySingleton(as: AuthPreferenceLocalDataSource)
class AuthPreferenceLocalDataSourceImpl
    implements AuthPreferenceLocalDataSource {
  const AuthPreferenceLocalDataSourceImpl(this._preference);

  final AuthSharedPreference _preference;
  @override
  Future<bool?> getLogInStatus() {
    return Future.value(_preference.getIsLoggedIn());
  }

  @override
  Future<bool> setLoggedIn() {
    return _preference.setIsLoggedIn();
  }

  @override
  Future<bool> setLoggedOut() {
    return _preference.removeIsLoggedIn();
  }
}
