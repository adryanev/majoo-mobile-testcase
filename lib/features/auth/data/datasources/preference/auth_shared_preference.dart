import 'package:injectable/injectable.dart';
import 'package:majootestcase/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class AuthSharedPreference {
  final _preference = getIt<SharedPreferences>();
  static const _isLoggedInKey = 'is_logged_in';

  Future<bool> setIsLoggedIn() async {
    return _preference.setBool(_isLoggedInKey, true);
  }

  Future<bool> removeIsLoggedIn() async {
    return _preference.setBool(_isLoggedInKey, false);
  }

  bool? getIsLoggedIn() {
    return _preference.getBool(_isLoggedInKey);
  }
}
