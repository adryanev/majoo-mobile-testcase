import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/core/domain/usecase.dart';
import 'package:majootestcase/features/auth/domain/entities/auth_failure.dart';
import 'package:majootestcase/features/auth/domain/repositories/auth_repository.dart';

@lazySingleton
class CheckAuthenticationStatus
    implements UseCase<AuthFailure, bool, NoParams> {
  const CheckAuthenticationStatus(this._repository);
  final AuthRepository _repository;

  @override
  Future<Either<AuthFailure, bool>> call(NoParams params) async {
    return _repository.checkIsUserLoggedIn();
  }
}
