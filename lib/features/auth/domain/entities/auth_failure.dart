import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/core/domain/failure/failures.dart';
part 'auth_failure.freezed.dart';

@freezed
class AuthFailure with _$AuthFailure {
  @Implements<LocalFailure>()
  const factory AuthFailure.userNotFound() = AuthUserNotFound;

  @Implements<LocalFailure>()
  const factory AuthFailure.cannotCreateUser() = AuthCannotCreateUser;

  @Implements<LocalFailure>()
  const factory AuthFailure.sessionFailure() = AuthSessionFailure;

  @Implements<UnexpectedFailure>()
  const factory AuthFailure.unexpectedFailure() = AuthUnexpectedFailure;
}
