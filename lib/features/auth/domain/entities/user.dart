import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/features/auth/domain/entities/value_objects.dart';
part 'user.freezed.dart';

@freezed
class User with _$User {
  const factory User({
    required int id,
    required EmailAddress email,
    required Username username,
    required Password password,
  }) = _User;
}
