import 'package:freezed_annotation/freezed_annotation.dart';
part 'serial.freezed.dart';

@freezed
class Serial with _$Serial {
  const factory Serial({
    required String title,
    required String? poster,
    String? duration,
  }) = _Serial;
}
