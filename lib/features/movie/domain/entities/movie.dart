import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/features/movie/domain/entities/serial.dart';
part 'movie.freezed.dart';

@freezed
class Movie with _$Movie {
  const factory Movie({
    required String title,
    required String? poster,
    required int year,
    List<Serial>? series,
  }) = _Movie;
}
