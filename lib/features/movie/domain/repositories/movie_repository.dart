import 'package:dartz/dartz.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/domain/failure/movie_failure.dart';

// ignore: one_member_abstracts
abstract class MovieRepository {
  Future<Either<MovieFailure, List<Movie>?>> getMovieList();
}
