import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/core/domain/failure/failures.dart';
part 'movie_failure.freezed.dart';

@freezed
class MovieFailure with _$MovieFailure {
  @Implements<MiddlewareFailure>()
  const factory MovieFailure.middlewareFailure() = MovieMiddlewareFailure;

  @Implements<ServerFailure>()
  const factory MovieFailure.serverFailure() = MovieServerFailure;
}
