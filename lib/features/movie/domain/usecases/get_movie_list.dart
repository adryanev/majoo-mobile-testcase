import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/core/domain/usecase.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/domain/failure/movie_failure.dart';
import 'package:majootestcase/features/movie/domain/repositories/movie_repository.dart';

@lazySingleton
class GetMovieList implements UseCase<MovieFailure, List<Movie>?, NoParams> {
  GetMovieList(this._repository);
  final MovieRepository _repository;
  @override
  Future<Either<MovieFailure, List<Movie>?>> call(NoParams params) {
    return _repository.getMovieList();
  }
}
