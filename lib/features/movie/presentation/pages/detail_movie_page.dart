import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/domain/entities/serial.dart';
import 'package:majootestcase/l10n/l10n.dart';

class DetailMoviePage extends StatelessWidget {
  const DetailMoviePage({Key? key, required this.movie}) : super(key: key);
  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(context.l10n.detailMovie)),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 12.h,
                ),
                Image.network(
                  movie.poster!,
                  height: 250.h,
                ),
                SizedBox(height: 8.h),
                Text(
                  movie.title,
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                SizedBox(
                  height: 6.h,
                ),
                Text(
                  movie.year.toString(),
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(height: 40.h),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Series",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                movie.series != null
                    ? SizedBox(
                        height: 200.h,
                        child: ListView.builder(
                            physics: const ClampingScrollPhysics(),
                            itemCount: movie.series?.length,
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemBuilder: (context, index) =>
                                SeriesItemWidget(serial: movie.series![index])),
                      )
                    : Text("No Series Found")
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SeriesItemWidget extends StatelessWidget {
  const SeriesItemWidget({Key? key, required this.serial}) : super(key: key);
  final Serial serial;
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Image.network(
              serial.poster!,
              height: 100.h,
              width: 100.h,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(serial.title, textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}
