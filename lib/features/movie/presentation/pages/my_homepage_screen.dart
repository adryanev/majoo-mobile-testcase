import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:majootestcase/app/routes/route_list.dart';
import 'package:majootestcase/features/auth/presentation/blocs/auth_status_bloc/auth_status_bloc.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/presentation/blocs/movie/movie_bloc.dart';
import 'package:majootestcase/injector.dart';
import 'package:majootestcase/l10n/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/shared/utils/helper.dart';
import 'package:majootestcase/shared/widgets/loading.dart';
import 'package:majootestcase/shared/widgets/movie_error_widget.dart';

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthStatusBloc, AuthStatusState>(
      listener: (context, state) {
        if (state is AuthStatusUnauthenticated) {
          context.go(login);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(context.l10n.appTitle),
          actions: [
            Padding(
              padding: EdgeInsets.all(8.r),
              child: InkWell(
                onTap: () {
                  context
                      .read<AuthStatusBloc>()
                      .add(const AuthStatusEvent.logout());

                  context
                    ..showSnackBar('Logged out')
                    ..go(login);
                },
                child: Icon(
                  Icons.logout,
                ),
              ),
            ),
          ],
        ),
        body: BlocProvider<MovieBloc>(
          create: (context) => getIt<MovieBloc>()
            ..add(
              const MovieEvent.fetchMovie(),
            ),
          child: BlocBuilder<MovieBloc, MovieState>(
            builder: (context, state) {
              return state.when(
                loading: () => const Center(
                  child: LoadingIndicator(),
                ),
                success: (movies) => ListView.builder(
                  itemBuilder: (context, index) =>
                      MovieItemWidget(movie: movies[index]),
                  itemCount: movies.length,
                ),
                failed: (failure) => failure.when(
                  middlewareFailure: () => MovieErrorWidget(
                    message: context.l10n.noConnection,
                    retry: () {
                      context
                          .read<MovieBloc>()
                          .add(const MovieEvent.fetchMovie());
                    },
                  ),
                  serverFailure: () => MovieErrorWidget(
                    message: context.l10n.cannotTalkToServer,
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class MovieItemWidget extends StatelessWidget {
  const MovieItemWidget({Key? key, required this.movie}) : super(key: key);
  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.push(detail, extra: movie);
      },
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network(movie.poster!),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(movie.title, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
