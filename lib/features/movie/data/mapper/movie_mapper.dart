import 'package:injectable/injectable.dart';
import 'package:majootestcase/features/movie/data/models/movies_response.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/domain/entities/serial.dart';

@lazySingleton
class MovieMapper {
  List<Movie> toDomain(List<Data> data) {
    return data
        .map(
          (each) => Movie(
            poster: each.info?.imageUrl,
            title: each.l!,
            year: each.year!,
            series: each.series
                ?.map(
                  (e) =>
                      Serial(title: e.l!, poster: e.i?.imageUrl, duration: e.s),
                )
                .toList(),
          ),
        )
        .toList();
  }
}
