import 'package:freezed_annotation/freezed_annotation.dart';
part 'movies_response.freezed.dart';
part 'movies_response.g.dart';

@freezed
class MoviesResponse with _$MoviesResponse {
  const factory MoviesResponse(
      {@JsonKey(name: 'd') List<Data>? data,
      @JsonKey(name: 'q') String? query,
      required int? v}) = _MoviesResponse;

  factory MoviesResponse.fromJson(Map<String, dynamic> json) =>
      _$MoviesResponseFromJson(json);
}

@freezed
class Data with _$Data {
  const factory Data({
    @JsonKey(name: 'i') Info? info,
    String? id,
    String? l,
    String? q,
    int? rank,
    String? s,
    @JsonKey(name: 'v') List<Series>? series,
    int? vt,
    @JsonKey(name: 'y') int? year,
    String? yr,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}

@freezed
class Info with _$Info {
  const factory Info({
    int? height,
    String? imageUrl,
    int? width,
  }) = _Info;
  factory Info.fromJson(Map<String, dynamic> json) => _$InfoFromJson(json);
}

@freezed
class Series with _$Series {
  const factory Series({
    Info? i,
    String? id,
    String? l,
    String? s,
  }) = _Series;

  factory Series.fromJson(Map<String, dynamic> json) => _$SeriesFromJson(json);
}
