import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/core/networks/utils/network_info.dart';
import 'package:majootestcase/features/movie/data/datasources/imdb_remote_data_source.dart';
import 'package:majootestcase/features/movie/data/mapper/movie_mapper.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/domain/failure/movie_failure.dart';
import 'package:majootestcase/features/movie/domain/repositories/movie_repository.dart';
import 'package:majootestcase/shared/utils/helper.dart';

@LazySingleton(as: MovieRepository)
class MovieRepositoryImpl implements MovieRepository {
  MovieRepositoryImpl(this._networkInfo, this._dataSource, this._mapper);
  final NetworkInfo _networkInfo;
  final ImdbRemoteDataSource _dataSource;
  final MovieMapper _mapper;
  @override
  Future<Either<MovieFailure, List<Movie>?>> getMovieList() async {
    try {
      if (await _networkInfo.isConnected) {
        const query = 'game of thr';
        final result = await _dataSource.fetchMovie(query);
        return right(_mapper.toDomain(result!.data!));
      }
      return left(const MovieFailure.middlewareFailure());
    } catch (ex) {
      logger.e(ex);
      return left(const MovieFailure.middlewareFailure());
    }
  }
}
