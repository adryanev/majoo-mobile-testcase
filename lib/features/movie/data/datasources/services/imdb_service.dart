import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/env_config.dart';
import 'package:majootestcase/features/movie/data/models/movies_response.dart';
import 'package:retrofit/retrofit.dart';
part 'imdb_service.g.dart';

@lazySingleton
@RestApi(baseUrl: EnvConfig.API_URL)
abstract class ImdbService {
  @factoryMethod
  factory ImdbService(Dio dio) = _ImdbService;

  @GET('auto-complete')
  Future<MoviesResponse> fetchMovie(@Query('q') String query);
}
