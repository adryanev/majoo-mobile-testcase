import 'package:injectable/injectable.dart';
import 'package:majootestcase/features/movie/data/datasources/services/imdb_service.dart';
import 'package:majootestcase/features/movie/data/models/movies_response.dart';

// ignore: one_member_abstracts
abstract class ImdbRemoteDataSource {
  Future<MoviesResponse?> fetchMovie(String query);
}

@LazySingleton(as: ImdbRemoteDataSource)
class ImdbRemoteDataSourceImpl implements ImdbRemoteDataSource {
  ImdbRemoteDataSourceImpl(this._service);
  final ImdbService _service;
  @override
  Future<MoviesResponse?> fetchMovie(String query) {
    return _service.fetchMovie(query);
  }
}
