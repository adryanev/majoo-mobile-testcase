import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:majootestcase/app/routes/route_list.dart';
import 'package:majootestcase/features/auth/presentation/blocs/auth_status_bloc/auth_status_bloc.dart';
import 'package:majootestcase/features/auth/presentation/pages/login/login_page.dart';
import 'package:majootestcase/features/auth/presentation/pages/register/register_page.dart';
import 'package:majootestcase/features/movie/domain/entities/movie.dart';
import 'package:majootestcase/features/movie/presentation/pages/detail_movie_page.dart';
import 'package:majootestcase/features/movie/presentation/pages/my_homepage_screen.dart';
import 'package:majootestcase/injector.dart';
import 'package:majootestcase/l10n/l10n.dart';
import 'package:majootestcase/shared/widgets/movie_error_widget.dart';
import 'package:majootestcase/shared/utils/constant.dart';
import 'package:majootestcase/shared/utils/helper.dart';

class App extends StatelessWidget {
  // This widget is the root of your application.
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authStatusBloc = getIt<AuthStatusBloc>()
      ..add(const AuthStatusEvent.authCheckRequested());
    late final router = GoRouter(
      initialLocation: login,
      refreshListenable: GoRouterRefreshStream(authStatusBloc.stream),
      redirect: (state) {
        if (authStatusBloc.state is AuthStatusInitial) return null;

        final loggedIn = authStatusBloc.state is AuthStatusAuthenticated;

        final goingToLogin = state.location == login;

        // the user is not logged in and not headed to /login, they need to login
        // if (!loggedIn && !goingToLogin) return login;

        // the user is logged in and headed to /login, no need to login again
        if (loggedIn && goingToLogin) return movie;

        // no need to redirect at all
        return null;
      },
      routes: [
        GoRoute(
          path: login,
          builder: (context, state) => const LoginPage(),
        ),
        GoRoute(
          path: register,
          builder: (context, state) => const RegisterPage(),
        ),
        GoRoute(
          path: movie,
          builder: (context, state) => const MyHomePageScreen(),
        ),
        GoRoute(
          path: detail,
          builder: (context, state) => DetailMoviePage(
            movie: state.extra! as Movie,
          ),
        ),
      ],
    );
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthStatusBloc>(
          create: (context) => authStatusBloc,
        )
      ],
      child: ScreenUtilInit(
        designSize:
            const Size(ScreenUtilConstants.width, ScreenUtilConstants.height),
        builder: () => MaterialApp.router(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          routerDelegate: router.routerDelegate,
          routeInformationParser: router.routeInformationParser,
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
          ],
          supportedLocales: AppLocalizations.supportedLocales,
          builder: (context, widget) {
            //add this line
            ScreenUtil.setContext(context);
            return MediaQuery(
              //Setting font does not change with system font size
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: widget!,
            );
          },
          // home: BlocProvider(
          //   create: (context) => AuthBlocCubit()..fetch_history_login(),
          // child: MyHomePageScreen(),
          // ),
        ),
      ),
    );
  }
}
