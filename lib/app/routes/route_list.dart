const login = '/login';
const register = '/register';

const movie = '/movies';
const detail = '/detail';
const movieDetail = movie + detail;

const error = '/error';
