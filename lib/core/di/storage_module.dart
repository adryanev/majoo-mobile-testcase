import 'package:injectable/injectable.dart';
import 'package:majootestcase/core/storage/app_database.dart';
import 'package:majootestcase/features/auth/data/services/dao/auth_dao.dart';
import 'package:majootestcase/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
abstract class StorageModule {
  @preResolve
  @lazySingleton
  Future<SharedPreferences> provideSharedPreference() =>
      SharedPreferences.getInstance();

  @preResolve
  @singleton
  Future<AppDatabase> provideAppDatabase() =>
      $FloorAppDatabase.databaseBuilder(AppDatabase.databaseName).build();

  @lazySingleton
  AuthDao get authDao => getIt<AppDatabase>().authDao;
}
