import 'dart:async';
import 'package:floor/floor.dart';
import 'package:majootestcase/features/auth/data/models/user_model.dart';
import 'package:majootestcase/features/auth/data/services/dao/auth_dao.dart';
// required package imports
import 'package:sqflite/sqflite.dart' as sqflite;

part 'app_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [UserModel])
abstract class AppDatabase extends FloorDatabase {
  static const databaseName = 'majootestcase.db';
  AuthDao get authDao;
}
