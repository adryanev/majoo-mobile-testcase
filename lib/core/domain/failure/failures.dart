abstract class Failure {
  const Failure([List properties = const <dynamic>[]]);
}

class ServerFailure extends Failure {}

class LocalFailure extends Failure {}

class MiddlewareFailure extends Failure {}

class UnexpectedFailure extends Failure {}
